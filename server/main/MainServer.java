package main;

import server.QueryHandler;

/**
 * Created by PianistaMichal on 04.01.2016.
 */
public class MainServer {
    public static final boolean debug = true;

    public static void main(String [] args) {
        System.out.println("Write 'help' to show help screen");
        QueryHandler queryHandler = new QueryHandler();
        queryHandler.main();
    }
}
