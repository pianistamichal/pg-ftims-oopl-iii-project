package server;

/**
 * Created by PianistaMichal on 13.01.2016.
 */
public interface Server {
    public void startServer();
    public void stopServer();
    public boolean statusServer();
}
