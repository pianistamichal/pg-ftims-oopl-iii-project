package server;

import data.Statistics;

import java.net.Socket;
import java.util.Scanner;

/**
 * Created by PianistaMichal on 03.01.2016.
 *
 * Class for managing all server classes, managing strategic points of server
 * It can start/stop online server, exit whole system, start/stop broadcasting conscious experience
 */
public class QueryHandler {
    private boolean mainServerWorking = true;
    private OnlineServer onlineServer;
    private Statistics data;
    private ConsciousServer consciousServer;

    public void startOnlineServer() {
        try {
            if(onlineServer != null) {
                stopOnlineServer();
            }
            data = new Statistics();
            onlineServer = new OnlineServer(data);
            onlineServer.startServer();
        } catch (Exception e) { }
    }

    public void stopOnlineServer() {
        try {
            onlineServer.stopServer();
        } catch (Exception e) { }
    }

    public boolean statusOnlineServer() {
        return onlineServer.statusServer();
    }

    public void startConsciousServer() {
        try {
            if(!statusOnlineServer()) {
                System.out.println("Start online server first");
            } else {
                stopConsciousServer();
                consciousServer = new ConsciousServer(onlineServer, data);
                consciousServer.startServer();
                onlineServer.sendToAll("start_conscious_experience");
            }
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void stopConsciousServer() {
        try {
            if(consciousServer != null) {
                consciousServer.stopServer();
                data.presentResults();
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public boolean statusConsciousServer() {
        return consciousServer.statusServer();
    }

    public void exit() {
        this.stopOnlineServer();
        this.stopConsciousServer();
        this.mainServerWorking = false;
    }

    public void main() {
        Scanner in = new Scanner(System.in);

        while(this.mainServerWorking) {
            this.execQuery(in.nextLine());
        }
    }

    public void execQuery(String arg) {
        if (arg.equals("start_online_server")) {
            this.startOnlineServer();
            execQuery("status_online_server");
        } else if (arg.equals("stop_online_server")) {
            this.stopOnlineServer();
            execQuery("status_online_server");
        } else if (arg.equals("status_online_server")) {
            System.out.println("Server online status is : " + (this.statusOnlineServer() ? "open" : "closed"));
        } else if(arg.equals("start_conscious_server")) {
            this.startConsciousServer();
            execQuery("status_conscious_server");
        } else if(arg.equals("stop_conscious_server")) {
            this.stopConsciousServer();
            execQuery("status_conscious_server");
        } else if(arg.equals("status_conscious_server")) {
            System.out.println("Conscious server status is : " + (this.statusConsciousServer() ? "started" : "not started"));
        } else if(arg.equals("users_online")) {
            onlineServer.writeAllUsersConnected();
        } else if(arg.equals("exit")) {
            this.exit();
        }  else if(arg.equals("help")) {
            System.out.println("Commands: ");
            System.out.println("start_online_server - to server online starts listening");
            System.out.println("stop_online_server - to server online stop listening");
            System.out.println("status_online_server - to server online status");
            System.out.println("start_conscious_server - to conscious server starts");
            System.out.println("stop_conscious_server - to conscious server stops");
            System.out.println("status_conscious_server - to conscious server status");
            System.out.println("help - to shows help");
            System.out.println("exit - to exit server");
        } else {
            System.out.println("I feel dumb but I don't know this command. Maybe look at 'help'? That's all I know. Sry.");
        }
    }
}