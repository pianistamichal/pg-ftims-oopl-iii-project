package server;

import data.Statistics;

import java.util.Date;
import java.util.Random;

/**
 * Created by PianistaMichal on 04.01.2016.
 *
 * Maintain broadcast conscious message to users
 */
public class ConsciousServer extends Thread implements Server {
    private boolean working = true;

    private OnlineServer onlineServer;
    private Statistics data;
    public static int time;

    public ConsciousServer(OnlineServer onlineServer, Statistics data) {
        this.onlineServer = onlineServer;
        this.data = data;
    }

    @Override
    public boolean statusServer() {
        return this.working;
    }

    @Override
    public void startServer() {
        working = true;
        this.start();
    }

    @Override
    public void stopServer() {
        try {
            this.working = false;
            this.join();
        }catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void run() {
        try {
            Random ran = new Random();
            while(this.working && onlineServer.amountOfConnected() > 0) {
                int randomTime = ran.nextInt(10000);
                int randUser = ran.nextInt(onlineServer.amountOfConnected());
                data.addNewTimeOfConsciousMessage(randomTime);
                data.addConsciousObserver(onlineServer.getConnectionByListNumber(randUser));

                time = 0;
                Date startTime = new Date();
                while(time < randomTime && this.working) {
                    Thread.sleep(10);
                    time = (int) (new Date().getTime() - startTime.getTime());
                }

                if(this.working) {
                    onlineServer.sendToSpecific("conscious_message", randUser);
                }

                while(time < 10000  && this.working) {
                    Thread.sleep(10);
                    time = (int) (new Date().getTime() - startTime.getTime());
                }
                data.endOfRound();
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            this.stopServer();
        }
    }
}
