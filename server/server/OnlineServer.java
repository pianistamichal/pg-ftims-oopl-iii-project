package server;

import data.Statistics;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PianistaMichal on 04.01.2016.
 *
 * Class for handle connection with all users
 * Contain list of all connection
 * Operations like: start listening on selected port for all connection,
 * stop listening, stop connection with selected connection
 */
public class OnlineServer extends Thread implements Server {
    private final int port = 100;
    private boolean working = true;

    private ServerSocket serverSocket;
    private Socket socket;
    private Statistics data;

    private List<Connection> listOfConnected;

    /*
    *   methods
     */
    public OnlineServer(Statistics data) {
        this.data = data;
    }

    public int amountOfConnected() {
        return listOfConnected.size();
    }

    public void sendToSpecific(String msg, int number) {
        listOfConnected.get(number).send(msg);
    }

    public void sendToSpecific(String msg, Connection con) {
        try {
            int lastIndexOf = listOfConnected.lastIndexOf(con);

            if(lastIndexOf != -1) {
                listOfConnected.get(lastIndexOf).send(msg);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }

    public void sendToAll(String msg) {
        for(Connection con : listOfConnected) {
            con.send(msg);
        }
    }

    public void endConnection(Connection con) {
        if(listOfConnected.contains(con)) {
            listOfConnected.remove(con);
        }
    }

    public Connection getConnectionByListNumber(int number) {
        return listOfConnected.get(number);
    }

    @Override
    public void startServer() {
        try {
            this.working = true;
            this.listOfConnected = new ArrayList<Connection>();
            serverSocket = new ServerSocket(port);
            serverSocket.setSoTimeout(300);
            this.start();
        } catch (Exception e) {
            System.out.println( e.getMessage() );
        }
    }

    @Override
    public void stopServer() {
        try {
            this.working = false;
            this.join();
            for(Connection con : listOfConnected) {
                con.closeSocket();
            }
            listOfConnected.clear();
            serverSocket.close();
        } catch (Exception e) {
            System.err.println( e.getMessage() );
        }
    }

    @Override
    public boolean statusServer() {
        return working;
    }

    public void writeAllUsersConnected() {
        for(Connection con : listOfConnected) {
            System.out.println(con.getIp());
        }
    }

    public void getNewConnection() {
        try {
            socket = serverSocket.accept();
            Connection newConnection = new Connection(socket, data, this);
            listOfConnected.add(newConnection);
            newConnection.start();

            data.addNewPerson(newConnection);
            System.out.println(socket.getLocalAddress().getHostAddress() + " connected");
        } catch (SocketTimeoutException e) {
            //normal situation - timeout
        } catch (Exception e) {
            System.err.println( e.getMessage() );
        }
    }

    @Override
    public void run() {
        while (this.working) {
            getNewConnection();
        }
    }
}
