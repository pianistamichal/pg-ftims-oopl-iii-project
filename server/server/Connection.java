package server;

import data.Statistics;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/*
*   Class for handle connection with one user
*   Operations like sending, receiving, closing connection
 */
public class Connection extends Thread {
    private OnlineServer onlineServer;
    private Statistics data;

    private String nick = "unnamed";

    private Socket socket;
    private boolean isSocketOpen = true;
    private PrintWriter out;
    private BufferedReader in;

    public Connection(Socket socket, Statistics data, OnlineServer onlineServer) {
        this.data= data;
        this.socket = socket;
        this.onlineServer = onlineServer;
        try {
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(
                    new InputStreamReader(socket.getInputStream())
            );
        } catch (Exception e) {
            System.err.println( e.getMessage() );
        }
    }

    public String getNick() {
        return this.nick;
    }

    public Socket getSocket() {
        return socket;
    }

    public void send(String msg) {
        out.println(msg);
    }

    public void closeSocket() {
        data.deletePerson(this);
        isSocketOpen = false;
        try {
            socket.close();
            out.close();
            in.close();
            onlineServer.endConnection(this);
        } catch (Exception e) {
            System.err.println( e.getMessage() );
        }
    }

    public String getIp() {
        return socket.getLocalAddress().getHostAddress();
    }

    @Override
    public void run() {
        while(isSocketOpen) {
            try {
                //sending message whatever what this message is
                String message = in.readLine();
                if(message.equals("key_pressed")) {
                    data.addTimeOfPressingKey(this);
                } else if(message.contains("set_nick")) {
                    String nick = message.substring(9);
                    this.nick = nick;
                }
            } catch (Exception e) {
                try {
                    System.out.println(socket.getLocalAddress().getHostAddress() + " disconnected");
                    closeSocket();
                } catch (Exception e1) { }
            }
        }
    }
}