package data;

import server.ConsciousServer;

import java.util.*;
import server.Connection;

/**
 * Created by PianistaMichal on 04.01.2016.
 */
public class Statistics {
    private Map<Connection, Statistic> allPeople;
    private long time;

    public Statistics() {
        allPeople = new TreeMap<Connection, Statistic>( new Comparator<Connection> () {
            @Override
            public int compare(Connection p1, Connection p2) {
                return p1.getName().compareTo(p2.getName());
            }
        }
        );
    }

    public void clear() {
        allPeople.clear();
    }

    public void addNewPerson(Connection connection) {
        try {
            allPeople.put(connection, new Statistic());
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public void deletePerson(Connection connection) {
        try {
            allPeople.remove(connection);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }

    }

    public void addNewTimeOfConsciousMessage(int time) {
        try {
            for(Map.Entry<Connection, Statistic> entry : allPeople.entrySet()) {
                entry.getValue().addTimeOfConsciousMessage(time);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public void addTimeOfPressingKey(Connection connection) {
        try {
            allPeople.get(connection).addTimeOfPressingKey(ConsciousServer.time);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public void endOfRound() {
        try {
            for(Map.Entry<Connection, Statistic> entry : allPeople.entrySet()) {
                entry.getValue().addTimeOfPressingKey(-1);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public void addConsciousObserver(Connection connection) {
        try {
            for(Map.Entry<Connection, Statistic> entry : allPeople.entrySet()) {
                if(entry.getKey().equals(connection)) {
                    entry.getValue().addIsInThisElementMessageWasSendToHim(true);
                } else {
                    entry.getValue().addIsInThisElementMessageWasSendToHim(false);
                }
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public void presentResults() {
        Map<Integer, String> leaderBoard = new TreeMap<Integer, String>();
        int amountOfTests = 0;
        double amountOfRightDecision = 0;
        int averageDeviation = 0;
        for(Map.Entry<Connection, Statistic> stat : allPeople.entrySet()) {
            amountOfRightDecision += stat.getValue().amountOfTimeHeHitRightSecond();
            amountOfTests = stat.getValue().amountOfConsciousMessages();
            leaderBoard.put(stat.getValue().averageDeviation(), stat.getKey().getNick());
            averageDeviation += stat.getValue().averageDeviation();
        }
        averageDeviation /= allPeople.size();

        System.out.println("Amount of people                    : " + allPeople.size());
        System.out.println("Amount of tests                     : " + amountOfTests);
        System.out.println("Average deviation                   : " + averageDeviation + " ms");
        System.out.println("Amount of strike home               : " + amountOfRightDecision);

        System.out.println("\nLeaderBoard:\n");
        int i = 0;
        for(Map.Entry<Integer, String> user : leaderBoard.entrySet()) {
            System.out.println(i++ + ": " + user.getValue() + " - " + user.getKey() + " ms");
        }
    }
}

class Statistic {
    private List<Integer> timeOfConsciousMessage;
    private List<Integer> timeOfPressingKey;
    private List<Boolean> isInThisElementMessageWasSendToHim;
    private String nickName;

    public Statistic() {
        timeOfConsciousMessage = new ArrayList<Integer>();
        timeOfPressingKey = new ArrayList<Integer>();
        isInThisElementMessageWasSendToHim = new ArrayList<Boolean>();
    }

    public Statistic(String nickName) {
        this();
        this.nickName = nickName;
    }

    public void addTimeOfConsciousMessage(int time) {
        this.timeOfConsciousMessage.add(time);
    }

    public void addTimeOfPressingKey(int time) {
        if(time == -1 && timeOfConsciousMessage.size() > timeOfPressingKey.size()) {
            this.timeOfPressingKey.add(time);
        } else if (time != -1) {
            this.timeOfPressingKey.add(time);
        }
    }

    public void addIsInThisElementMessageWasSendToHim(boolean val) {
        this.isInThisElementMessageWasSendToHim.add(val);
    }

    public int amountOfTimeHeHitRightSecond() {
        int toReturn = 0;

        for(int i = 0; i<timeOfPressingKey.size(); i++) {
            if(!isInThisElementMessageWasSendToHim.get(i)) {
                if(Math.abs(timeOfConsciousMessage.get(i) - timeOfPressingKey.get(i)) < 1000) {
                    toReturn++;
                }
            }
        }
        return toReturn;
    }

    public int averageDeviation() {
        int toReturn = 0;

        for(int i = 0; i<timeOfPressingKey.size(); i++) {
            if(!isInThisElementMessageWasSendToHim.get(i)) {
                toReturn += Math.abs(timeOfConsciousMessage.get(i) - timeOfPressingKey.get(i));
            }
        }

        toReturn /= timeOfPressingKey.size();

        return toReturn;
    }

    public int amountOfConsciousMessages() {
        return timeOfConsciousMessage.size();
    }
}