package Graphic;

import Connection.QueryHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;


/**
 * Created by PianistaMichal on 04.01.2016.
 */
public class ConsciousClient implements Client{
    private final int INTERVAL = 100;
    private boolean working = true;

    private QueryHandler queryHandler;
    private Timer timer;
    private int consciousMessageCounter = 0;
    private int timerCount = 0;

    public ConsciousClient(QueryHandler queryHandler) {
        this.queryHandler = queryHandler;
    }

    @Override
    public void startClient() {
        this.working = true;
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
    }

    @Override
    public void stopClient() {
        this.working = false;
    }

    @Override
    public boolean statusClient() {
        return this.working;
    }

    public void keyPressed() {
        queryHandler.keyPressed();
    }

    public void consciousMessageArrived() {
        consciousMessageCounter = 10;
    }

    private String randomString(int length) {
        Random rand = new Random();
        String toReturn = "";
        for(int i = 0; i<length; i++) {
            toReturn += Character.toString((char)(65 + rand.nextInt(26)));
        }

        return toReturn;
    }

    public void createAndShowGUI() {
        Window window = new Window(this);

        JFrame f = new JFrame("Swiadomy test v 1.0.0 - Michal Blocinski");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.add(window);
        f.addKeyListener(window);
        f.pack();
        f.setVisible(true);

        timer = new Timer(INTERVAL, new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if(consciousMessageCounter > 0) {
                    window.setText("YOU HAVE BEEN CHOOSEN");
                    consciousMessageCounter--;
                } else {
                    window.setText(randomString(30));
                }
                timerCount++;
                if(timerCount > 9) {
                    window.setSecond(window.getSecond() + 1);
                    if(window.getSecond() > 9) {
                        window.setSecond(0);
                    }
                    timerCount = 0;
                }

                //Refresh the panel
                window.repaint();

                if (!queryHandler.statusConsciousExperience()) {
                    timer.stop();
                }
            }
        });

        timer.start();
    }
}

class Window extends JPanel implements KeyListener{
    private ConsciousClient consciousClient;
    private final int width = 100*16;
    private final int height = 100*9;

    private int whichSecond = 0;
    private boolean keySecondPressed = false;
    private String text = "ABC";

    public Window(ConsciousClient consciousClient) {
        this.consciousClient = consciousClient;

        this.setBorder(BorderFactory.createLineBorder(Color.black));
        this.addKeyListener(this);
        this.setFocusable(true);
    }

    public void setSecond(int second) {
        if(second == 0) {
            keySecondPressed = false;
        }
        whichSecond = second;
    }

    public int getSecond() {
        return whichSecond;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(width, height);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g.create();
        g2d.setFont(new Font("Courier New", Font.BOLD, 48));
        FontMetrics fm = g2d.getFontMetrics();
        int x = ((getWidth() - fm.stringWidth(text)) / 2);
        int y = ((getHeight() - fm.getHeight()) / 2) + fm.getAscent();

        g2d.setColor(Color.BLACK);
        g2d.drawString(text, x, y);

        String textToDraw = whichSecond + "/10";
        g2d.setFont(new Font("Courier New", Font.BOLD, 20));
        fm = g2d.getFontMetrics();
        x = (getWidth() - fm.stringWidth(textToDraw) - 20);
        y = fm.getHeight()  + 20;
        g2d.drawString(textToDraw, x, y);

        textToDraw = "Key: " + (keySecondPressed ? "already used" : "not used");
        x = (getWidth() - fm.stringWidth(textToDraw) - 20);
        y = 2 * fm.getHeight() + 20;
        g2d.drawString(textToDraw, x, y);

        g2d.dispose();
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(!keySecondPressed) {
            consciousClient.keyPressed();
            keySecondPressed = true;
        }
    }

    @Override
    public void keyTyped(KeyEvent e) { }

    @Override
    public void keyReleased(KeyEvent e) { }
}