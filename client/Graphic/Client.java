package Graphic;

/**
 * Created by PianistaMichal on 13.01.2016.
 */
public interface Client {
    public void startClient();
    public void stopClient();
    public boolean statusClient();
}
