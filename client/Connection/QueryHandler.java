package Connection;

import Graphic.ConsciousClient;

import java.util.Scanner;

/**
 * Created by PianistaMichal on 05.01.2016.
 */
public class QueryHandler {
    private boolean serverWorking = true;
    private ConnectionMaintain connectionMaintain;
    private ConsciousClient consciousClient;
    private String nick = "unnamed";

    public QueryHandler() {
        Scanner in = new Scanner(System.in);

        while(serverWorking) {
            this.execQuery(in.nextLine());
        }
    }

    public void exit() {
        this.serverWorking = false;
    }

    public String getNick() {
        return nick;
    }

    public void createConnection(String ip) {
        try {
            disconnect();

            connectionMaintain = new ConnectionMaintain(this, ip);
            connectionMaintain.start();
        } catch (Exception e) { }
    }

    public void keyPressed() {
        connectionMaintain.sendToServer("key_pressed");
    }

    public void disconnect() {
        try {
            if(statusConsciousExperience()) {
                stopConsciousExperience();
            }

            if(connectionMaintain != null) {
                connectionMaintain.disconnect();
            }
        } catch(Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void startConsciousExperience() {
        consciousClient = new ConsciousClient(this);
        consciousClient.startClient();
    }

    public void stopConsciousExperience() {
        consciousClient.stopClient();
    }

    public boolean statusConsciousExperience() {
        return consciousClient.statusClient();
    }

    public void consciousMessageArrived() {
        consciousClient.consciousMessageArrived();
    }

    public void execQuery(String arg) {
        if(arg.contains("set_nick")) {
            String nick = arg.substring(9);
            this.nick = nick;
            if(connectionMaintain != null) {
                connectionMaintain.sendToServer("set_nick " + nick);
            }

            System.out.println("You nick is: " + nick);
        } else if (arg.contains("connect ")) {
            String ip = arg.substring(8);
            this.createConnection(ip);
        } else if(arg.equals("disconnect")) {
            this.disconnect();
        } else if(arg.equals("exit")) {
            this.exit();
        } else if(arg.equals("help")) {
            System.out.println("Commands: ");
            System.out.println("set_nick <nickname> - to set You nickname, otherwise You will be unnamed");
            System.out.println("connect <ip_address> - to connect to server by given ip address");
            System.out.println("disconnect - to disconnect from server");
            System.out.println("exit - to stop program");
        } else {
            System.out.println("I feel dumb but I don't know this command. Maybe look at 'help'? That's all I know. Sry.");
        }
    }

    public void apiQuery(String arg) {
        if(arg.equals("start_conscious_experience")) {
            this.startConsciousExperience();
        } else if(arg.equals("conscious_message")) {
            this.consciousMessageArrived();
        }
    }
}
