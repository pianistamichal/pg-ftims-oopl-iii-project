package Connection;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created by PianistaMichal on 04.01.2016.
 */
public class ConnectionMaintain extends Connection {
    private final int port = 100;
    private boolean isConnectionOpen = true;

    private QueryHandler queryHandler;

    private Socket socket;
    private String ipAddress;
    private PrintWriter out;
    private BufferedReader in;

    public ConnectionMaintain(QueryHandler queryHandler, String ipAddress) {
        this.queryHandler = queryHandler;
        this.ipAddress = ipAddress;
    }

    public void sendToServer(String msg) {
        out.println(msg);
    }

    @Override
    public void disconnect() throws DisconnectException {
        try {
            socket = null;
            in = null;
            out = null;
            isConnectionOpen = false;
            this.join();
            throw new DisconnectException();
        } catch(Exception e) {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void connect() {
        if(!ipAddress.isEmpty()) {
            try {
                socket = new Socket();
                socket.connect(new InetSocketAddress(ipAddress, port), 1000);
                System.out.println("Connected to: " + ipAddress);
                isConnectionOpen = true;
                out = new PrintWriter(socket.getOutputStream(), true);
                in = new BufferedReader(
                        new InputStreamReader(
                                socket.getInputStream()
                        )
                );

            } catch (Exception e) {
                System.out.println("Cannot connect");
                try {
                    disconnect();
                } catch (Exception e1) { }

            }
        }
    }

    public void run() {
        connect();
        sendToServer("set_nick " + queryHandler.getNick());
        while(isConnectionOpen) {
            try {
                String msg = in.readLine();
                if(msg.equals(null)) {
                    disconnect();
                } else {
                    //apiQuery recognize msg
                    queryHandler.apiQuery(msg);
                }
            } catch (Exception e) {
                try {
                    disconnect();
                } catch (Exception e1) { }
            }
        }
    }
}