package Connection;

/**
 * Created by PianistaMichal on 13.01.2016.
 */
public abstract class Connection extends Thread{
    public abstract void connect();
    public abstract void disconnect() throws DisconnectException;
}
