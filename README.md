# Object-Oriented Programming Languages III final project #

## Basic informations
* Author: Michał Błociński
* Start date: 03.01.16
* Mail: PianistaMichal@gmail.com
* All rights reserved

## Project informations
* Project to investigate human consciousness
* Quantum theory
* Examine if one human can affect other human brain via thought
* Project separated into server/client (instructions later)
* Every 10 sec server will send message to one person from all that connect, this message will be exacly "You have been choosen"
* This message will be shown on client side to this person that server randomly choosed
* Every connected user can press space key to mark second from 10 sec that he thinks message has arrived to somebody
* Mathematic probability is equals to 10% (exact second from 10 s) and I'm conscious of that fact

## Requirements
* jre 1.8
* path to java runner have to be declared as environment path
* port 100 open

## How to compile
Project is already compiled in folders client/out/ and server/out/ to recompile files just use google

## How to run
* run "run.bat" file from client/out/ or server/out/ to run client/server side program

## How to use server side
* write help in console to learn more
* first start server
* write "start_online_server"
* wait for all users
* write "start_conscious_server"
* when You choose time to end and show statistics write "stop_conscious_server" and there will be statistics with leaderboard

## How to use client side
* write help in console to learn more
* write "set_nick <nick>" to change You nickname
* write "connect <ip_address>" to connect to server
* when server side will start conscious_server there will be shown JFrame window, and press space when You think message arrives to somebody
* exit window when server has used "stop_conscious_server" query